set nocompatible               " be iMproved

" NeoBundle
if has('vim_starting')
    set runtimepath+=~/.vim/bundle/neobundle.vim/
endif
" call neobundle#rc(expand('~/.vim/bundle/'))
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'Shougo/vimproc', { 'build': {
      \   'windows': 'make -f make_mingw32.mak',
      \   'cygwin': 'make -f make_cygwin.mak',
      \   'mac': 'make -f make_mac.mak',
      \   'unix': 'make -f make_unix.mak',
      \ } }

" Revision Control
NeoBundle 'tpope/vim-fugitive'

" File Searching
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'scrooloose/nerdtree'
" NeoBundle 'Shougo/unite.vim'

" Commenting
NeoBundle 'tomtom/tcomment_vim'

" Text Objects
NeoBundle 'tpope/vim-surround'

" Motions
NeoBundle 'Lokaltog/vim-easymotion'

" Theming
NeoBundle 'flazz/vim-colorschemes'

" Syntax Checking
" NeoBundle 'scrooloose/syntastic'

" Jedi. install with 'pip install jedi'
" NeoBundle 'davidhalter/jedi-vim'
" NeoBundle 'Valloric/YouCompleteMe'

" Status Line
NeoBundle 'bling/vim-airline'
NeoBundle 'bling/vim-bufferline'

call neobundle#end()

filetype plugin indent on
" end NeoBundle config

" Enter NERDTree if no file is given
autocmd vimenter * if !argc() | NERDTree | endif

" Remaps
let mapleader= ','
" Exit insert mode with a quick jj
inoremap jj <ESC>
" Clear the highlight in normal mode
nnoremap <leader>n :noh<CR>

inoremap <C-a> <Home>
inoremap <C-e> <End>

syntax on

set hidden                      " Hide buffers when they are abandoned
set showmatch                   " Show matching brackets.


" Formatting
set nowrap                      " Wrap long lines
set autoindent                  " Indent at the same level of the previous line
set smartindent
set smarttab
set shiftwidth=4                " Use indents of 4 spaces
set expandtab                   " Tabs are spaces, not tabs
set tabstop=4                   " An indentation every four columns
set softtabstop=4               " Let backspace delete indent
set pastetoggle=<F12>           " pastetoggle

set incsearch                   " Find the next match as we type the search
set hlsearch                    " Hilight searches by default
set viminfo='100,f1             " Save up to 100 marks, enable capital marks

set wildmode=list:longest
set wildmenu                    " Enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~     " Stuff to ignore when tab completing

set number
set backspace=2

"------------------------------------------------------------------------------
" CTags
" With this ctags will recurse up directories looking for tags files
set tags=tags;/

"------------------------------------------------------------------------------
" CScope
if has("cscope")
    set cst

    function! LoadCscope()
      let db = findfile("cscope.out", ".;")
      if (!empty(db))
        let path = strpart(db, 0, match(db, "/cscope.out$"))
        set nocscopeverbose " suppress 'duplicate connection' error
        exe "cs add " . db . " " . path
        set cscopeverbose
      endif
    endfunction
    au BufEnter /* call LoadCscope()

endif

"------------------------------------------------------------------------------
" For airline
set t_Co=256 " Explicitly tell Vim that the terminal supports 256 colors
set laststatus=2
set noshowmode
let g:airline_symbols = get(g:, 'airline_symbols', {})
let g:airline_theme='badwolf'
let g:airline_left_sep = ')'
let g:airline_right_sep = '('

set encoding=utf-8
if has("multi_byte")
    " let g:airline_left_sep = '▶'
    " let g:airline_right_sep = '◀'
    let g:airline_symbols.branch = '⎇ '
endif
let g:bufferline_echo = 0

if !has('gui_running')
    set ttimeoutlen=10
    " augroup FastEscape
    "     autocmd!
    "     au InsertEnter * set timeoutlen=0
    "     au InsertLeave * set timeoutlen=1000
    " augroup END
endif

" if has('mouse')
"     set mouse=a
" end

"------------------------------------------------------------------------------
" Colorscheme
if has('gui_running')
  if has("gui_macvim")
    set guifont=Source\ Code\ Pro:h14
  elseif has("gui_gtk2")
    set guioptions-=T           "remove toolbar
    set guioptions-=r           "remove right-hand scroll bar
    set guioptions-=l           "remove left-hand scroll bar
    set guioptions-=b           "remove bottom scroll bar
    set guifont=Source\ Code\ Pro\ 14
  end

  set background=dark
  colorscheme twilight
else
    colorscheme twilight256
endif

highlight ExtraWhitespace ctermbg=red guibg=red
" match ExtraWhitespace /\s\+$/
match ExtraWhitespace /\s\+$\| \+\ze\t/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Local vimrc
if filereadable(glob("~/.vimrc.local"))
    source ~/.vimrc.local
endif

